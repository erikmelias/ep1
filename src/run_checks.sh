#!/bin/bash

# Função para verificar se uma string contém outra string
contains_string() {
    string="$1"
    substring="$2"
    if [[ "$string" == *"$substring"* ]]; then
        return 0 # Contém a substring
    else
        return 1 # Não contém a substring
    fi
}

# Verifica se o programa "./check" retorna a palavra "DIFERENTE" para uma determinada combinação de parâmetros
check_program() {
    grid_size="$1"
    num_threads="$2"
    start_time=$(date +%s.%N)

    output=$(./check --grid_size "$grid_size" --num_threads "$num_threads")
    end_time=$(date +%s.%N)
    elapsed_time=$(echo "scale=3; $end_time - $start_time" | bc)
    if contains_string "$output" "DIFERENTE"; then
        echo "ERRO EM CHECK com grid de $grid_size e $num_threads threads: ./check  --grid_size $grid_size  --num_threads $num_threads"
        exit 1
    else
        echo "Demorou $elapsed_time segundos, mas tudo certo com : ./check  --grid_size $grid_size  --num_threads $num_threads"

    fi
}

#Vai recompilar tudo, para garantir que esteja ok.
clear
make clean
make

start_gtime=$(date +%s.%N)

# Loop para executar todas as combinações de grid_size e num_threads
for ((num_threads = 1; num_threads <= 32; num_threads *= 2)); do
    for ((grid_size = 32; grid_size <= 4096; grid_size *= 2)); do
        check_program "$grid_size" "$num_threads"
        sleep 0.5
    done
done

end_gtime=$(date +%s.%N)
elapsed_gtime=$(echo "scale=3; $end_gtime - $start_gtime" | bc)

echo "Demorou um total de $elapsed_gtime segundos para execução do script e não foram encontrados erros."

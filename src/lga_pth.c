#include "lga_base.h"
#include "lga_pth.h"
#include <pthread.h> // Incluindo para poder usar Pthreads

// struct para simplificar a passagem de parâmetros na hora de chamar as partes paralelas.
typedef struct
{
    int thread;
    int start;
    int end;
    int grid_size;
    byte *grid_out;
    byte *grid_in;
} ThreadArgs;

static byte get_next_cell(int i, int j, byte *grid_in, int grid_size)
{
    byte next_cell = EMPTY;

    for (int dir = 0; dir < NUM_DIRECTIONS; dir++)
    {
        int rev_dir = (dir + NUM_DIRECTIONS / 2) % NUM_DIRECTIONS;
        byte rev_dir_mask = 0x01 << rev_dir;

        int di = directions[i % 2][dir][0];
        int dj = directions[i % 2][dir][1];
        int n_i = i + di;
        int n_j = j + dj;

        if (inbounds(n_i, n_j, grid_size))
        {
            if (grid_in[ind2d(n_i, n_j)] == WALL)
            {
                next_cell |= from_wall_collision(i, j, grid_in, grid_size, dir);
            }
            else if (grid_in[ind2d(n_i, n_j)] & rev_dir_mask)
            {
                next_cell |= rev_dir_mask;
            }
        }
    }

    return check_particles_collision(next_cell);
}

// Basicamente faz o que o UPDATE fazia, só que em partes conforme o número de trheads
void *update_chunk(void *arg)
{
    ThreadArgs *args = (ThreadArgs *)arg;

    int grid_size = args->grid_size; // Para não alterar a Macro ind2d, fazemos essa variável.

    for (int i = args->start; i < args->end; i++)
    {
        for (int j = 0; j < grid_size; j++)
        {
            if (args->grid_in[ind2d(i, j)] == WALL)
                args->grid_out[ind2d(i, j)] = WALL;
            else
                args->grid_out[ind2d(i, j)] = get_next_cell(i, j, args->grid_in, grid_size);
        }
    }
    pthread_exit(NULL);
}

// O método UPDATE foi modificado para ser paralelo em partes, e por isso usamos o num_threads como parämetrou
static void update(byte *grid_in, byte *grid_out, int grid_size, int num_threads)
{
    int num_chunks = num_threads;       // Quantidade de partes para dividir os grids.
    pthread_t threads[num_chunks];      // Array de threads
    ThreadArgs thread_args[num_chunks]; // Array de argumentos das threads

    int chunk_size = grid_size / num_chunks; // Tamanho de cada chunk
    int start, end;                          // início e fim delimitando cada parte proessada
    for (int chunk = 0; chunk < num_chunks; chunk++)
    {
        // definindo os parâmetros, que serão usadas em cada threads

        start = chunk * chunk_size;
        end = start + chunk_size;

        thread_args[chunk].thread = chunk;
        thread_args[chunk].start = start;
        thread_args[chunk].end = end;
        thread_args[chunk].grid_size = grid_size;
        thread_args[chunk].grid_out = grid_out;
        thread_args[chunk].grid_in = grid_in;

        // Definido os parâmtros, lança cada thread para executar uma parte do UPDATE
        pthread_create(&threads[chunk], NULL, update_chunk, (void *)&thread_args[chunk]);
        grid_out = thread_args[chunk].grid_out;
    }

    for (int chunk = 0; chunk < num_chunks; chunk++)
    {
        pthread_join(threads[chunk], NULL);
    }
}

void simulate_pth(byte *grid_1, byte *grid_2, int grid_size, int num_threads)
{
    for (int i = 0; i < ITERATIONS / 2; i++)
    {
        update(grid_1, grid_2, grid_size, num_threads); // Foi alterada a assinatura do método UPDATE, para que fosse possível passar a quantidade de trheads desejadas.
        update(grid_2, grid_1, grid_size, num_threads);
    }
}

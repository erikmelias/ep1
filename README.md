# EP1

## Checking if the implementation is correct!

To verify the correctness of the coding phase, a script has been created to execute the `./check` program with different combinations of `grid_size` and `num_thread`.

The proposed script will first perform a `make clean` command, followed by a `make` command. This will ensure that the entire code is compiled and ready for execution. Please disregard the output of the terminal during this process, or evaluate it if you encounter any compilation errors or warnings. 

After compilation, the script will automatically start the checking phase by varying the `grid_size` and `num_threads`. The `--grid_size` parameter will start from **32** and go up to **4096**, increasing in powers of two. Similarly, `num_threads` will range from **1** to **32**, also increasing in powers of two.

The `run_checks.sh` script should be placed in the same folder as the `./check` program, typically in the `src` folder. To run the script, simply execute the following command:
```
./run_checks.sh
```

Make sure that the script has execution permissions. If needed, you can grant the permissions using the command:
```
chmod +x run_checks.sh
```

The expected output for each `./check` execution is:
```
Demorou [S] segundos, mas tudo certo com : ./check  --grid_size [G]  --num_threads [T]
```
Here, [S] represents the time taken for a single `./check` execution, [G] represents the tested `grid_size`, and [T] represents the tested `num_threads`.

During the script execution, if an error is found, the script will display:
```
ERRO EM CHECK com grid de 2048 e 16 threads: ./check  --grid_size [G]  --num_threads [T]
```
Here, [G] represents the tested `grid_size`, and [T] represents the tested `num_threads`.

If no errors are found, the final output will be:
```
Demorou um total de [S] segundos para execução do script e não foram encontrados erros.
```
Here, [S] represents the total time taken to run the script.

## Implementation details

The code includes both OpenMP and Pthreads implementations. Please refer to the code for more details.

